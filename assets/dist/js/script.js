document.getElementById('about-dropdown').addEventListener('click', function(){
	if(screen.width < 992){
		if(document.getElementById('about-menu-dropdown').style.display === '' || document.getElementById('about-menu-dropdown').style.display === 'none'){
			document.getElementById('about-menu-dropdown').style.display = 'block'
			document.getElementById('service-menu-dropdown').style.display = ''
			document.getElementById('page-menu-dropdown').style.display = ''
			document.getElementById('blog-dropdown').style.display = ''
		} else {
			document.getElementById('about-menu-dropdown').style.display = ''
		}
	} else{
		document.getElementById('about-menu-dropdown').style.display = ''
		document.getElementById('service-menu-dropdown').style.display = ''
		document.getElementById('page-menu-dropdown').style.display = ''
		document.getElementById('blog-dropdown').style.display = ''
	}
})
document.getElementById('service-dropdown').addEventListener('click', function(){
	if(screen.width < 992){
		if(document.getElementById('service-menu-dropdown').style.display === '' || document.getElementById('service-menu-dropdown').style.display === 'none'){
			document.getElementById('service-menu-dropdown').style.display = 'block'
			document.getElementById('page-menu-dropdown').style.display = ''
			document.getElementById('about-menu-dropdown').style.display = ''
			document.getElementById('blog-dropdown').style.display = ''
		} else {
			document.getElementById('service-menu-dropdown').style.display = ''
		}
	} else{
		document.getElementById('about-menu-dropdown').style.display = ''
		document.getElementById('service-menu-dropdown').style.display = ''
		document.getElementById('page-menu-dropdown').style.display = ''
		document.getElementById('blog-dropdown').style.display = ''
	}
})
document.getElementById('page-dropdown').addEventListener('click', function(){
	if(screen.width < 992){
		if(document.getElementById('page-menu-dropdown').style.display === '' || document.getElementById('page-menu-dropdown').style.display === 'none'){
			document.getElementById('page-menu-dropdown').style.display = 'block'
			document.getElementById('about-menu-dropdown').style.display = ''
			document.getElementById('service-menu-dropdown').style.display = ''
		} else {
			document.getElementById('page-menu-dropdown').style.display = ''
		}
	} else{
		document.getElementById('about-menu-dropdown').style.display = ''
		document.getElementById('service-menu-dropdown').style.display = ''
		document.getElementById('page-menu-dropdown').style.display = ''
		document.getElementById('blog-dropdown').style.display = ''
	}
})
document.getElementById('icon-blog-dropdown-responsive').addEventListener('click', function(){
	if(screen.width < 992){
		if(document.getElementById('blog-dropdown').style.display === '' || document.getElementById('blog-dropdown').style.display === 'none'){
			document.getElementById('blog-dropdown').style.display = 'block'
		} else {
			document.getElementById('blog-dropdown').style.display = ''
		}
	} else{
		document.getElementById('about-menu-dropdown').style.display = ''
		document.getElementById('service-menu-dropdown').style.display = ''
		document.getElementById('page-menu-dropdown').style.display = ''
		document.getElementById('blog-dropdown').style.display = ''
	}
})

document.getElementById('toggle-menu').addEventListener('click', function(){
	document.getElementById('about-menu-dropdown').style.display = ''
	document.getElementById('service-menu-dropdown').style.display = ''
	document.getElementById('page-menu-dropdown').style.display = ''
	document.getElementById('blog-dropdown').style.display = ''

	if(document.getElementById('menu-header').style.display === ''){
		document.getElementById('menu-header').style.display = 'block'
	} else{
		document.getElementById('menu-header').style.display = ''
	}
})